<?php

namespace App\Http\Controllers;

use App\Models\Cinema;
use App\Models\Countries;
use App\Models\Singer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.index', ['result' => (new \App\Models\Singer)->index()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add', ['countries' => Countries::all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $singer = new Singer();
        $singer->country_id = $request->input('country');
        $singer->name = $request->input('singer');
        $singer->song_name = $request->input('song');
        $singer->rate = $request->input('balu');
        $singer->save();
        return redirect()->route('singers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.edit', ['find' => (new Singer())->showById($id),'countries' => Countries::all()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('singers')
            ->where('id', $id)
            ->update([
                'country_id' => $request->input('country'),
                'name' => $request->input('singer'),
                'song_name' => $request->input('song'),
                'rate' => $request->input('balu')]);
        return redirect()->route('singers.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Singer::where('id', $id)->delete();
        return redirect()->route('singers.index');
    }
}
