<?php

namespace App\Http\Controllers;

use App\Models\Bibiki;
use Illuminate\Http\Request;

class SingerController extends Controller
{
    public function index(Request $request)
    {
        return view('guest.index', ['result' => (new \App\Models\Singer)->index($request->input('sort'))]);
    }

    public function show(Request $request)
    {
        return view('guest.find', ['result' => (new \App\Models\Singer)->show($request->input('singr_name'))]);
    }
}
