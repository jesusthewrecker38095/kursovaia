<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Singer extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function index($sort = null)
    {
        $result = DB::table('singers')
            ->join('countries', 'countries.id', '=', 'singers.country_id')
            ->select('singers.song_name', 'singers.id as singr_id', 'singers.rate',
                'singers.name as singr_name', 'countries.*');

        switch ($sort) {
            case 1:
                return $result->orderBy('singers.rate')->get();
                break;
            case 2:
                return $result->orderBy('singers.rate', 'desc')->get();
                break;
            default:
                return $result->orderBy('singers.rate','desc')->get();
                break;
        }
    }

    public function show($name)
    {
        return DB::table('singers')
            ->join('countries', 'countries.id', '=', 'singers.country_id')
            ->select('singers.*', 'singers.id as singr_id', 'singers.name as singr_name', 'countries.*')
            ->where('singers.name', 'like', "%$name%")
            ->get();
    }

    public function showById($id)
    {
        return DB::table('singers')
            ->join('countries', 'countries.id', '=', 'singers.country_id')
            ->select('singers.*', 'singers.id as singr_id', 'singers.name as singr_name', 'countries.*')
            ->where('singers.id',  "$id")
            ->get();
    }
}
