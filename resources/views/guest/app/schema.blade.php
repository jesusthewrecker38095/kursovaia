<!doctype html>
<html lang="en">
<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
</head>
<body>
<center><h2>EuroVision</h2></center>
@yield('content')
@if(isset($result))
    @foreach($result as $row)
        <div class="card mb-4 shadow-sm">
            <div class="card-header">
                <h4 class="my-0 fw-normal">{{$row->name}}</h4>
            </div>
            <div class="card-body">

                <ul class="list-unstyled mt-3 mb-4">
                    <li>Бали: {{$row->rate}}</li>
                    <li>Ім'я співака: {{$row->singr_name}}</li>
                    <li>Пісня: {{$row->song_name}}</li>
                </ul>
            </div>
        </div>
    @endforeach
@endif
</body>
</html>


