@extends('guest.app.schema')

@section('title','Home')

@section('content')
    <p>
        <a href="{{route('find')}}">Poisk</a>
        <a href="{{ route('login') }}">{{ __('Login') }}</a>
    </p>
    <p>
    <form action="{{route('index')}}">
        @csrf
        <label for="sort">Sort type:</label>
        <select name="sort" id="sort">
            <option value="1">Place top</option>
            <option value="2">Place down</option>
        </select>
        <button type="submit">Sort</button>
    </form>
    </p>
@endsection
