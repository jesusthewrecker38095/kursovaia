@extends('guest.app.schema')

@section('title','Poisk')

@section('content')
    <p>
        <a href="{{route('index')}}">Go home</a>
    </p>
    <p>
    <form action="{{route('show')}}">
        @csrf
        <label for="make">Ім'я:</label>
        <input type="text" id="singr_name" name="singr_name">
        <button>Пошук</button>
    </form>
    </p>
@endsection
