@extends('admin.schema')
@foreach($find as $el)
    @section('title','Edit '.$el->name)
@section('content')

    <div class="container">
        <form action="{{route('singers.update',['id' => $el->singr_id])}}" method="post">
            <input name="_method" type="hidden" value="PUT">
            @csrf
            <div class="row g-3">
                <div class="col-sm-6">
                    <label for="firstName" class="form-label">Singer name</label>
                    <input type="text" class="form-control" id="firstName" placeholder="" value="{{$el->singr_name}}"
                           required=""
                           name="singer">
                    <div class="invalid-feedback">
                        Valid singer name is required.
                    </div>
                </div>

                <div class="col-sm-6">
                    <label for="songName" class="form-label">Song name</label>
                    <input type="text" class="form-control" id="songName" placeholder="" value="{{$el->song_name}}"
                           required=""
                           name="song">
                    <div class="invalid-feedback">
                        Valid Song name is required.
                    </div>
                </div>
                <div class="col-sm-6">
                    <label for="Balu" class="form-label">Balu</label>
                    <input type="text" class="form-control" id="Balu" placeholder="" value="{{$el->rate}}" required=""
                           name="balu">
                    <div class="invalid-feedback">
                        Valid Balu is required.
                    </div>
                </div>

                <div class="col-md-5">
                    <label for="country" class="form-label">Country</label>
                    <select class="form-select" id="country" required="" name="country">
                        @foreach($countries as $country)
                            <option value='{{ $country->id }}'>{{$country->name}}</option>
                        @endforeach
                    </select>
                    <div class="invalid-feedback">
                        Please select a valid country.
                    </div>
                </div>

            </div>
            <button class="w-100 btn btn-primary btn-lg" type="submit">EDIT</button>
        </form>
    </div>

@endsection
@endforeach
