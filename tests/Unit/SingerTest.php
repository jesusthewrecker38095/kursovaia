<?php

namespace Tests\Unit;

use \App\Singer;
use PHPUnit\Framework\TestCase;

class SingerTest extends TestCase
{
    protected function setUp(): void
    {
        $this->singer = new Singer();
    }
    /**
     * @dataProvider rateDataProvider
     * @param $actual
     * @param $expected
     */
    public function testRate($actual, $expected)
    {
        $result = $this->rate = $actual;
        $this->assertEquals($expected, $result);
    }
    public function rateDataProvider(): array
    {
        return array(
            array(120, 120),
            array(47, 47),
            array(438, 438)
        );
    }
    protected function tearDown(): void
    {
    }
    public function testStub()
    {
        $stub = $this->getMockBuilder(Singer::class)
            ->disableOriginalConstructor()
            ->disableOriginalClone()
            ->getMock();

        $stub->method('getSongName')
            ->willReturn( 'My Freedom');

        $this->assertSame("My Freedom", $stub->getSongName());
    }
}
