<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\SingerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [SingerController::class, 'index'])->name('index');

Route::get('/find', function () {
    return view('guest.find');
})->name('find');

Route::get('/find/result', [SingerController::class, 'show'])->name('show');

Route::middleware('auth')->group(function () {

    Route::resource('admin/singers', 'App\Http\Controllers\AdminController',
        ['parameters' =>[
            'singers' => 'id']]
    );

});

Auth::routes([
    'register' => false,
    'verify' => false,
    'reset' => false
]);

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
